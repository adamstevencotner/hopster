# The Hopster Application

### Extra Features ###

* Individual user accounts with ability
* Publicly accessible API for multi-platform integration
* Mobile-friendly website (check it out)

### What I would do with more time (features) ###

* Search by ingredient
* Edit and delete user-specific recipes
* Share via email, social media, beer forums (plugin dependent)
* Login redirection to intended target

### What I would do with more time (code) ###

* Nested routing for recipe box rendering
* Modularize login checking
* Cloak loading on recipe pages
* Allow CORS for Auth API endpoints (outside of /token)
* Re-implement ingredient sorting in recipe creation process
* I realize now that the API controller for serving recipes is misspelled. Should be RecipeController.