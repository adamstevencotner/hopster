﻿'use strict';

var Hopster = angular.module('Hopster', ['ngResource', 'ngRoute', 'ngCookies'])
    .config(function ($routeProvider) {

        $routeProvider
            .when('/',
            {
                templateUrl: 'views/home.html',
                controller: 'home'
            })
            .when('/login',
            {
                templateUrl: 'views/login.html',
                controller: 'login'
            })
            .when('/logout',
            {
                templateUrl: 'views/logout.html',
                controller: 'logout'
            })
            .when('/profile',
            {
                templateUrl: 'views/profile.html',
                controller: 'profile'
            })
            .when('/recipe/:id',
            {
                templateUrl: 'views/recipe.html',
                controller: 'recipe'
            })
            .when('/about',
            {
                templateUrl: 'views/about.html',
                controller: 'about'
            })
            .otherwise({ redirectTo: '/' });

    });

// used for testing locally before loading up to Azure
// would not be in production code
var API_ENDPOINT = '';
if (location.hostname == 'localhost') {
    API_ENDPOINT = 'http://localhost:50938/';
} else {
    API_ENDPOINT = 'http://hopster.azurewebsites.net/api/';
}