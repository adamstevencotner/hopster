﻿Hopster.filter('orderIngredients', function () {
    return function (items) {
        var filtered = [];
        angular.forEach(items, function (item) {
            filtered.push(item);
        });
        filtered.sort(function (a, b) {
            return (a.name > b.name ? 1 : -1);
        });
        return filtered;
    };
});