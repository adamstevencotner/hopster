﻿'use strict';

Hopster.factory('DataService', function ($resource) {
    return {
        getrecipes: function (token) {
            if (!token || token == "") {
                return $resource(API_ENDPOINT + 'recipes', null, {
                    all: {
                        method: "GET",
                        isArray: true
                    }
                });
            } else {
                return $resource(API_ENDPOINT + 'recipes/User', null, {
                    all: {
                        method: "GET",
                        headers: { 'Authorization': 'bearer ' + token },
                        isArray: true
                    }
                });
            }
        },
        getrecipeById: function (id) {
            return $resource(API_ENDPOINT + 'recipes?id='+ id, null, {
                all: {
                    method: "GET",
                    isArray: true
                }
            });
        },
        submitrecipe: function (token) {
            return $resource(API_ENDPOINT + 'recipes', null, {
                all: {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'bearer ' + token
                    }
                }
            })
        },
        searchrecipes: function (searchString) {
            return $resource(API_ENDPOINT + 'recipes/Search?q=' + searchString, null, {
                all: {
                    method: "GET",
                    isArray: true
                }
            });
        },
        getIngredients: function (token) {
            return $resource(API_ENDPOINT + 'Ingredients', null, {
                all: {
                    method: "GET",
                    headers: { 'Authorization': 'bearer ' + token },
                    isArray: true
                }
            });
        }
    }
});