﻿'use strict';

Hopster.factory('AuthService', function ($resource) {
    return {
        login: $resource(API_ENDPOINT + 'token', null, {
            token: {
                method: "POST",
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                // loads data into encoded form format
                transformRequest: function (data, headersGetter) {
                    var str = [];
                    for (var d in data)
                        str.push(encodeURIComponent(d) + "=" + encodeURIComponent(data[d]));

                    return str.join("&");
                }
            }
        }),
        registration: $resource(API_ENDPOINT + 'account/register', null, {
            register: {
                method: "POST",
                headers: { 'Content-Type': 'application/json' }
            }
        })
    }
});