﻿'use strict';

// used to persist user data across controllers
Hopster.factory('User', function () {
    return {
        token: '',
        username: ''
    };
});