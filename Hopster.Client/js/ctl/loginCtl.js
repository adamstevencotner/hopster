﻿'use strict';

Hopster.controller('login', function ($scope, $rootScope, AuthService, User, $cookies, $location) {

    $scope.loginError = {
        exists: false
    };

    $scope.submit_login = function () {
        User.username = $scope.username;
        AuthService.login.token(
            {
                username: User.username,
                password: $scope.password,
                grant_type: 'password'
            },
            function (data) {
                $scope.loginError = {
                    exists: false
                };
                // token setting
                User.token = data.access_token;
                $cookies.putObject('User', User);
                $rootScope.logLink = '/#/logout';
                $rootScope.logText = 'Log Out';

                $location.path('/');
            },
            function (resp) {
                $scope.loginError = {
                    exists: true,
                    text: resp.data.error_description
                };
            });
    }

    // register a new user
    $scope.submit_register = function () {
        User.username = $scope.username;
        AuthService.registration.register(
            {
                email: User.username,
                password: $scope.password,
                confirmPassword: $scope.password
            },
            function (data) {
                $scope.submit_login();
            },
            function (resp) {
                $scope.loginError = {
                    exists: true,
                    text: resp.data.modelState[""][0]
                };
            });
    }

    $scope.close = function () {
        $location.path('/');
    }
});