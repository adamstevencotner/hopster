﻿'use strict';

Hopster.controller('home', function ($scope, $rootScope, $timeout, DataService, User, $location, $cookies) {

    // login aesthetics
    if (!$cookies.get('User')) {
        $rootScope.logLink = '/#/login';
        $rootScope.logText = 'Log In';
    } else {
        $rootScope.logLink = '/#/logout';
        $rootScope.logText = 'Log Out';
        User = JSON.parse($cookies.get('User'));
    }

    $scope.searchTriggered = false;
    $scope.searchByName = function (e) {

        // prevent keybounce/searching on every keystroke
        if (!$scope.searchTriggered) {
            $scope.searchTriggered = true;

            // wait 500 ms between searches
            $timeout(function () {

                // allow next search
                $scope.searchTriggered = false;

                var searchString = e.target.value;

                // prevent an empty search box from triggering search
                if (searchString && searchString != "") {
                    DataService.searchrecipes(searchString).all(
                        function (data) {
                            $scope.recipes = data;
                        },
                        function (resp) {
                            console.log(resp);
                        });
                } else {
                    DataService.getrecipes().all(
                        function (data) {
                            $scope.recipes = data;
                        },
                        function (resp) {
                            console.log(resp)
                        });
                }
            }, 250);
        }
    }

    // force execution of GET on page load.
    $scope.searchByName({target:{value:false}});
});