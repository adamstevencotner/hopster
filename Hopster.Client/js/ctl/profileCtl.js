﻿'use strict';

Hopster.controller('profile', function ($scope, $rootScope, DataService, User, $location, $cookies, $route) {

    var pageInit = function () {
        $scope.username = User.username;
        $scope.newrecipe = false;
        $scope.recipeIngredients = [];
        $scope.toggle_recipe = function () {
            $scope.newrecipe = !$scope.newrecipe;
        }

        // load existing recipes
        DataService.getrecipes(User.token).all(
            function (data) {
                $scope.recipes = data;
            },
            function (resp) {
                console.log(resp)
            });
        // load existing ingredients
        DataService.getIngredients(User.token).all(
            function (data) {
                $scope.availableIngredients = data;
            },
            function (resp) {
                console.log(resp)
            });

        $scope.add_ingredient = function () {
            // add the ingredient to the list for the recipe
            $scope.recipeIngredients.push($scope.availableIngredients[$scope.selectedIngredientIndex]);
            // set the proper amount of that ingredient
            $scope.recipeIngredients[$scope.recipeIngredients.length - 1].amount = $scope.selectedIngredientAmount;
            // clear the ingredient amount from the scope
            $scope.selectedIngredientAmount = "";
            // remove the ingredient from the list of available ingredients
            $scope.availableIngredients.splice($scope.selectedIngredientIndex, 1);
        }

        $scope.remove_ingredient = function (ingredient, index) {
            // remove the amount property
            delete ingredient.amount;
            // add the ingredient back to the list of avaialble ingredients
            $scope.availableIngredients.push(ingredient);
            // remove the ingredient from the list of included ingredients
            $scope.recipeIngredients.splice(index, 1);
            $scope.selectedIngredientIndex = "";
        }

        $scope.submit_recipe = function () {

            // regroup into recipeInfo format
            var formattedIngredients = [];
            var newIngr = {};
            for (var i = 0; i < $scope.recipeIngredients.length; i++) {
                newIngr = {};
                newIngr["ingredient"] = {};
                newIngr["ingredient"]["name"] = $scope.recipeIngredients[i].name;
                newIngr["amount"] = $scope.recipeIngredients[i].amount;
                formattedIngredients.push(newIngr);
            }

            // submit the new recipe
            DataService.submitrecipe(User.token).all(
                {
                    "recipeInfoList": formattedIngredients,
                    "name": $scope.recipeName,
                    "description": $scope.recipeDescription,
                    "instructions": $scope.recipeInstructions
                },
                function (data) {
                    $route.reload();
                },
                function (resp) {
                    alert("Uh-Oh!");
                });
        }
    }

    // login aesthetics
    if (!$cookies.get('User')) {
        $rootScope.logLink = '/#/login';
        $rootScope.logText = 'Log In';
        $location.path('/login');
    } else {
        $rootScope.logLink = '/#/logout';
        $rootScope.logText = 'Log Out';
        User = JSON.parse($cookies.get('User'));

        pageInit();
    }
});