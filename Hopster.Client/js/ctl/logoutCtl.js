﻿'use strict';

Hopster.controller('logout', function ($scope, $rootScope, $timeout, $cookies, $location) {

    $cookies.remove('User');
    $rootScope.logLink = '/#/login';
    $rootScope.logText = 'Log In';
    $timeout(function () {
        $location.path('/');
    }, 1000);

});