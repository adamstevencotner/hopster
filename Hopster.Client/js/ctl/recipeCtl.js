﻿'use strict';

Hopster.controller('recipe', function ($scope, $rootScope, $routeParams, $cookies, User, DataService) {
    
    // login aesthetics
    if (!$cookies.get('User')) {
        $rootScope.logLink = '/#/login';
        $rootScope.logText = 'Log In';
    } else {
        $rootScope.logLink = '/#/logout';
        $rootScope.logText = 'Log Out';
        User = JSON.parse($cookies.get('User'));
    }

    // load recipe info for the id in the URI
    DataService.getrecipeById($routeParams.id).all(
        function (data) {
            $scope.recipe = data[0];
        },
        function (resp) {
            console.log(resp)
        });

});