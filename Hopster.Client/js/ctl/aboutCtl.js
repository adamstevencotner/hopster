﻿'use strict';

Hopster.controller('about', function ($scope, $rootScope, User, $cookies) {

    // login aesthetics
    if (!$cookies.get('User')) {
        $rootScope.logLink = '/#/login';
        $rootScope.logText = 'Log In';
    } else {
        $rootScope.logLink = '/#/logout';
        $rootScope.logText = 'Log Out';
        User = JSON.parse($cookies.get('User'));
    }
});