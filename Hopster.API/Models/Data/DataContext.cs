﻿using System.Data.Entity;

namespace Hopster.Models.Data
{
    /// <summary>
    /// Creates connection to datasource (Web.Config) and accessible database entities.
    /// </summary>
    public class DataContext : DbContext
    {
        public DataContext() : base("name=DataConnection") { }

        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Ingredient> Ingredients { get; set; }
        public DbSet<RecipeInfo> RecipeInfos { get; set; }
        public DbSet<Unit> Units { get; set; }
    }
}