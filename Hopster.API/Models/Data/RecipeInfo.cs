﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Hopster.Models.Data
{
    /// <summary>
    /// Container for an individual ingredient and its associated amount.
    /// </summary>
    public class RecipeInfo
    {
        [JsonIgnore]
        public int Id { get; set; }
        [Required]
        public double Amount { get; set; }
        
        [JsonIgnore]
        public virtual Recipe Recipe { get; set; }
        [Required]
        public virtual Ingredient Ingredient { get; set; }
    }
}