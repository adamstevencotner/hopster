﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hopster.Models.Data
{
    /// <summary>
    /// Represents a Unit of measure, such as Ounces, Fluid Ounces, etc.
    /// </summary>
    public class Unit
    {
        [JsonIgnore]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        [JsonIgnore]
        public virtual List<Ingredient> Ingredient { get; set;}
    }
}