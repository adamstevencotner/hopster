﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Hopster.Models.Data
{
    /// <summary>
    /// Represents the container for a beer recipe.
    /// </summary>
    public class Recipe
    {
        //[JsonIgnore]
        public int Id { get; set; }
        // Not required, but set programmatically
        public string OwnerName { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Instructions { get; set; }

        [Required]
        public virtual List<RecipeInfo> RecipeInfoList { get; set; }
    }
}