﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Hopster.Models.Data
{
    /// <summary>
    /// Represents the ingredients for beer, such as hops, yeast, water, love, etc.
    /// </summary>
    public class Ingredient
    {
        [JsonIgnore]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }

        // TO DO: how to require property when adding ingredients, ignore when adding recipes
        //[Required]
        //[JsonIgnore]
        public virtual Unit Unit { get; set; }
    }
}