﻿using System.Collections.Generic;
using System.Data.Entity;

/* THIS FILE TO BE EXCLUDED FROM THE PROJECT AFTER SUCCESSFUL CREATION OF TABLES */

namespace Hopster.Models.Data
{
    /// <summary>
    /// Seeding not only adds some initial items to the database, but creates
    /// the required tables according the established models.
    /// 
    /// This file would not be included in production code.
    /// </summary>
    public class DataContextInit : DropCreateDatabaseIfModelChanges<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            // seed units
            var units = new List<Unit>()
            {
                new Unit()
                {
                    Name = "Ounces"
                },
                new Unit()
                {
                    Name = "Fluid Ounces"
                },
                new Unit()
                {
                    Name = "Count"
                }
            };

            // seed ingredients
            var ingredients = new List<Ingredient>()
            {
                new Ingredient()
                {
                    Name = "Galaxy Hops",
                    Unit = units[0]
                },
                new Ingredient()
                {
                    Name = "Grain",
                    Unit = units[0]
                },
                new Ingredient()
                {
                    Name = "Yeast",
                    Unit = units[0]
                },
                new Ingredient()
                {
                    Name = "Water",
                    Unit = units[1]
                }
            };

            // seed one recipe
            var recipes = new List<Recipe>()
            {
                new Recipe()
                {
                    Name = "Test Recipe",
                    OwnerName = "adamstevencotner@gmail.com",
                    Description = "So far, it's a little crunchy",
                    Instructions = "You pour hops into a glass."
                }
            };

            // seed one recipeinfo
            var recipeinfos = new List<RecipeInfo>()
            {
                new RecipeInfo()
                {
                    Amount = 4.55,
                    Ingredient = ingredients[0],
                    Recipe = recipes[0]
                },
                new RecipeInfo()
                {
                    Amount = 2,
                    Ingredient = ingredients[1],
                    Recipe = recipes[0]
                },
                new RecipeInfo()
                {
                    Amount = 60,
                    Ingredient = ingredients[3],
                    Recipe = recipes[0]
                }
            };

            foreach (Unit u in units)
            {
                context.Units.Add(u);
            }
            foreach (Ingredient i in ingredients)
            {
                context.Ingredients.Add(i);
            }
            foreach (Recipe r in recipes)
            {
                context.Recipes.Add(r);
            }
            foreach (RecipeInfo ri in recipeinfos)
            {
                context.RecipeInfos.Add(ri);
            }

            context.SaveChanges();

            base.Seed(context);
        }
    }
}