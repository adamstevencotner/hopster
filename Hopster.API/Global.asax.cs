﻿using Hopster.Models.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Hopster.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            // Code-First approach to creating Data tables.
            // This line to be commented out when tables are successfully created.
            Database.SetInitializer(new DataContextInit());

            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
