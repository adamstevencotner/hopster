﻿using System;
using System.Web.Http;
using Hopster.Models.Data;
using Hopster.API.Repos;

namespace Hopster.Controllers
{
    /// <summary>
    /// Handles all calls to the /ingredients endpoint.
    /// </summary>
    [Authorize]
    [RoutePrefix("Ingredients")]
    public class IngredientsController : ApiController
    {
        private IngredientsRepo ir = new IngredientsRepo();

        /// <summary>
        /// Get handler for all ingredients
        /// </summary>
        /// <returns>
        /// list of custom ingredients objects. See IngredientsRepo.getAll()
        /// </returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetIngredients()
        {
            try
            {
                var results = ir.getAll();
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get ingredients whos name contains the searchString
        /// </summary>
        /// <returns>
        /// List of custom ingredient objects. See IngredientsRepo.getIngredientsByName()
        /// </returns>
        [HttpGet]
        [Route("Search")]
        [Authorize]
        public IHttpActionResult GetIngredientsByname(string q)
        {
            try
            {
                var results = ir.getIngredientByName(q);
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Post handler for adding ingredients
        /// </summary>
        /// <param name="ingredient">
        /// Ingredient Model
        /// </param>
        /// <returns>
        /// 201 Created
        /// </returns>
        [HttpPost]
        [Route("")]
        public IHttpActionResult PostIngredient(Ingredient ingredient)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                ir.addIngredient(ingredient);

                return Created("", new { name = ingredient.Name });

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                ir.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}