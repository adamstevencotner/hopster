﻿using System;
using System.Web.Http;
using Hopster.Models.Data;
using Hopster.API.Repos;
using Microsoft.AspNet.Identity;
using System.Web.Http.Cors;

namespace Hopster.Controllers
{
    /// <summary>
    /// Handles all calls to the /recipes endpoint
    /// </summary>
    [RoutePrefix("Recipes")]
    public class RecipesController : ApiController
    {
        RecipesRepo rr = new RecipesRepo();

        /// <summary>
        /// Auth-less call for all Recipes in the database.
        /// </summary>
        /// <returns>
        /// List of custom recipes objects. See RecipesRepo.getAll()
        /// </returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetRecipes()
        {
            try
            {
                var results = rr.getAll();
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Auth-less call for all Recipes in the database.
        /// </summary>
        /// <returns>
        /// List of custom recipes objects. See RecipesRepo.getAll()
        /// </returns>
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetRecipeById(int id)
        {
            try
            {
                var results = rr.getRecipeById(id);
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get recipes belonging to the current user
        /// </summary>
        /// <returns>
        /// List of custom recipes objects. See RecipesRepo.getByUser()
        /// </returns>
        [HttpGet]
        [Route("User")]
        [Authorize]
        public IHttpActionResult GetRecipesByUser()
        {
            try
            {
                // User.Identity.GetUserName() identifies the user by their token
                var results = rr.getByUser(User.Identity.GetUserName());
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Get recipes whos name contains the searchString
        /// </summary>
        /// <returns>
        /// List of custom recipes objects. See RecipesRepo.getRecipeByName()
        /// </returns>
        [HttpGet]
        [Route("Search")]
        //[Authorize]
        public IHttpActionResult GetRecipesByname(string q)
        {
            try
            {
                var results = rr.getRecipeByName(q);
                return Ok(results);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Post handler at the /recipes endpoint
        /// </summary>
        /// <param name="recipe">
        /// Recipe model
        /// </param>
        /// <returns>
        /// 201 Status
        /// </returns>
        [Route("")]
        [HttpPost]
        [Authorize]
        public IHttpActionResult PostRecipe(Recipe recipe)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                string userName = User.Identity.GetUserName();
                rr.addRecipe(recipe, userName);

                return Created("", new { name = recipe.Name });

            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                rr.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}