﻿using Hopster.Models.Data;

namespace Hopster.API.Repos
{
    /// <summary>
    /// Parent class to all repositories accessing the DataContext
    /// </summary>
    public class Repository
    {
        // In a scalable applicaiton, DataContext would be a parent class
        // to be overwritten by the child repositories.
        protected DataContext db = new DataContext();

        public void Dispose()
        {
            db.Dispose();
        }
    }
}