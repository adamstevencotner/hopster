﻿using Hopster.Models.Data;
using System.Collections.Generic;
using System.Linq;

namespace Hopster.API.Repos
{
    /// <summary>
    /// Contains all functions necessary for preparing data entering and
    /// exiting the /ingredients endpoint
    /// </summary>
    public class IngredientsRepo : Repository
    {
        /// <summary>
        /// Gets all the ingredients from the database.
        /// </summary>
        /// <returns>
        /// Custom ingredients objects
        /// </returns>
        public List<Ingredient> getAll()
        {
            return db.Ingredients.ToList();
        }

        /// <summary>
        /// Gets all ingredients who's name matches the searchString.
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns>
        /// Matching ingredient objects
        /// </returns>
        public List<Ingredient> getIngredientByName(string searchString)
        {
            return (from ingredient in db.Ingredients
                    where ingredient.Name.Contains(searchString)
                    select ingredient).ToList();
        }

        /// <summary>
        /// Add a single ingredient to the Database
        /// </summary>
        /// <param name="ingredient">
        /// Ingredient Model
        /// </param>
        public void addIngredient(Ingredient ingredient)
        {
            db.Ingredients.Add(ingredient);
            db.SaveChanges();

            return;
        }
    }
}