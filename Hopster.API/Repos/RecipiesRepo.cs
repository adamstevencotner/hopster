﻿using Hopster.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hopster.API.Repos
{
    /// <summary>
    /// Contains all functions necessary for preparing data entering and
    /// exiting the /recipes endpoint
    /// </summary>
    public class RecipesRepo : Repository
    {
        /// <summary>
        /// Gets all the recipes from the database.
        /// </summary>
        /// <returns>
        /// All recipe objects
        /// </returns>
        public List<Recipe> getAll()
        {
            return db.Recipes.ToList();
        }

        /// <summary>
        /// Gets all recipes submitted be the user.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>
        /// Your recipe objects
        /// </returns>
        public List<Recipe> getByUser(string userName)
        {
            return db.Recipes.Where(r => r.OwnerName == userName).ToList();
        }

        /// <summary>
        /// Gets all recipes who's name matches the searchString.
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns>
        /// Matching recipe objects
        /// </returns>
        public List<Recipe> getRecipeByName(string searchString)
        {
            return (from recipe in db.Recipes
                   where recipe.Name.Contains(searchString)
                   select recipe).ToList();
        }

        /// <summary>
        /// Gets all recipes who's id matches the query.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>
        /// Matching recipe object
        /// </returns>
        public List<Recipe> getRecipeById(int id)
        {
            return (from recipe in db.Recipes
                    where recipe.Id == id
                    select recipe).ToList();
        }

        /// <summary>
        /// Add a single recipe to the database
        /// </summary>
        /// <param name="recipe"></param>
        /// <param name="userName"></param>
        public void addRecipe(Recipe recipe, string userName)
        {
            // Set OwnerName to current user's name. Added bonues: helps prevent injection.
            recipe.OwnerName = userName;

            // Find Ingredient by name, or throw an error if no ingredient with that name exists
            List<Ingredient> ingredientList = new List<Ingredient>();
            foreach(RecipeInfo ri in recipe.RecipeInfoList)
            {
                var matchedIngredient = db.Ingredients.Where(e => e.Name == ri.Ingredient.Name).First();
                if (matchedIngredient == null)
                {
                    throw new Exception(String.Format("No Ingredient '{0}' Found.", ri.Ingredient.Name));
                }
                else
                {
                    ri.Ingredient = matchedIngredient;
                }
            }
            db.Recipes.Add(recipe);
            db.SaveChanges();

            return;
        }
    }
}